from app import models
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from werkzeug.security import generate_password_hash

DB_NAME = "database.sqlite"
engine = create_engine(f'sqlite:///instance/{DB_NAME}')

Session = sessionmaker(bind=engine)
session = Session()

new_user = models.User(
  email='me@onestringlab.com', 
  first_name='Onestring', 
  last_name='Lab', 
  role='admin', 
  password=generate_password_hash('secret'),
  created_at = datetime.utcnow(),
  updated_at = datetime.utcnow())

session.add(new_user)
session.commit()
