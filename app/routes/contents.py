from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify

# create Blueprint
contents = Blueprint('contents', __name__)

@contents.route('/', methods=['GET', 'POST'])
def index():
  return render_template('home.html', user=current_user)

@contents.route('/home', methods=['GET', 'POST'])
def home():
  return render_template('home.html', user=current_user)