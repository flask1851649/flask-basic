from app import db
from app.models import Log
from datetime import datetime
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for

# create Blueprint
cruds = Blueprint('cruds', __name__)

@cruds.route('/crud-gen', methods=['GET', 'POST'])
@login_required
def crud_gen():
  if request.method == "GET":
    fields = ['id', 'label', 'created_at', 'updated_at']
    table = "label"
    model = "Label"
    add_html = "<form method=\"POST\" action=\"/"+table+"-add\">\n"
    # <div class="mb-3">
    #   <label for="data" class="form-label fw-bold">Data</label>
    #   <textarea name="data" id="data" class="form-control"></textarea>
    # </div>
    for field in fields:
      add_html += "<div class=\"mb-3\">\n"
      add_html += "   <label for=\""+field+"\" class=\"form-label fw-bold\">"+field+"</label>\n"
      add_html += "   <input type=\"text\" class=\"form-control\" name=\""+field+"\" id=\""+field+"\" placeholder=\"Enter "+field+"\">\n"
      add_html += "</div>\n"
    add_html += "<div class=\"mb-3\">\n"
    add_html += "<a class=\"btn btn-sm btn-secondary\" role=\"button\" href=\"/"+table+"-list\">Back</a>\n"
    add_html += "<button type=\"submit\" class=\"btn btn-sm btn-primary\">Add "+table+"</button>\n"
    add_html += "</div>\n"
    add_html += "</form>"

    edit_html = "<form method=\"POST\" action=\"/"+table+"-edit/{{ data."+fields[0]+" }}\">\n"
    for field in fields:
      edit_html += "<div class=\"mb-3\">\n"
      edit_html += "   <label for=\""+field+"\" class=\"form-label fw-bold\">"+field+"</label>\n"
      edit_html += "   <input type=\"text\" class=\"form-control\" name=\""+field+"\" id=\""+field+"\" value=\"{{ data."+field+" }}\">\n"
      edit_html += "</div>\n"
    edit_html += "<div class=\"mb-3\">\n"
    edit_html += "  <input type=\"hidden\" name=\"id\" value=\"{{ data."+fields[0]+" }}\">\n"
    edit_html += "  <a class=\"btn btn-sm btn-secondary\" role=\"button\" href=\"/"+table+"-list\">Back</a>\n"
    edit_html += "  <button type=\"submit\" class=\"btn btn-sm btn-primary\">Edit "+table+"</button>\n"
    edit_html += "</div>\n"
    edit_html += "</form>"

    del_html = "<form method=\"POST\" action=\"/"+table+"-del/{{ data."+fields[0]+" }}\">\n"
    for field in fields:
      del_html += "<div class=\"mb-3\">\n"
      del_html += "   <label for=\""+field+"\" class=\"form-label fw-bold\">"+field+"</label>\n"
      del_html += "   <input type=\"text\" class=\"form-control\" name=\""+field+"\" id=\""+field+"\" value=\"{{ data."+field+" }}\" readonly>\n"
      del_html += "</div>\n"
    del_html += "<div class=\"mb-3\">\n"
    del_html += "  <input type=\"hidden\" name=\"id\" value=\"{{ data."+fields[0]+" }}\">\n"
    del_html += "  <a class=\"btn btn-sm btn-secondary\" role=\"button\" href=\"/"+table+"-list\">Back</a>\n"
    del_html += "  <button type=\"submit\" class=\"btn btn-sm btn-primary\">Delete "+table+"</button>\n"
    del_html += "</div>\n"
    del_html += "</form>"

    list_html = "<table class=\"table table-sm\">\n"
    list_html += "<thead>\n"
    list_html += "<tr class=\"table-primary\">\n"
    for field in fields:
      list_html += "<th class=\"text-center\">"+field+"</th>\n"
    list_html += "<th width=\"90\" class=\"text-center\">\n"
    list_html += "<a class=\"btn btn-sm btn-primary\" role=\"button\" href=\"/"+table+"-add\">\n"
    list_html += "<i class=\"fa-solid fa-plus\"></i></a>\n"
    list_html += "</th>\n"
    list_html += "</tr>\n"
    list_html += "</thead>\n" 
    list_html += "<tbody>\n"
    list_html += "{% set count = namespace(value=1) %}\n"
    list_html += "{% for "+table+" in " + table +"s %}\n"
    list_html += "<tr>\n"
    for field in fields:
      list_html += "<td>{{ "+table+"."+ field + " }}</td>\n"
    list_html += "<td class=\"text-center\">\n"
    list_html += "<a class=\"btn btn-sm btn-secondary\" role=\"button\" href=\"/"+table+"-edit/{{ "+table+".id }}\">\n"
    list_html += "<i class=\"fa-solid fa-pen\"></i></a>\n"
    list_html += "<a class=\"btn btn-sm btn-danger\" role=\"button\" href=\"/"+table+"-del/{{ "+table+".id }}\">\n"
    list_html += "<i class=\"fa-solid fa-trash\"></i></a>\n"
    list_html += "</td>\n"
    list_html += "</tr>\n"
    list_html += "{% set count.value = count.value + 1 %}\n"
    list_html += "{% endfor%}\n"
    list_html += "</tbody>\n"
    list_html += "</table>\n"

    blueprint = table +"s = Blueprint('"+table+"s', __name__)"
    import_blueprint = "from .routes."+table+"s import "+table+"s\n"
    register_blueprint = "app.register_blueprint("+table+"s, url_prefix='/')"

    routes = "@"+table+"s.route('/"+table+"-add', methods=['GET', 'POST'])\n"
    routes += "@login_required\n"
    routes += "def "+table+"_add():\n"
    for field in fields:
      routes += "  "+field+" = request.form.get('"+field+"')\n"
    routes += "  if request.method == 'POST':\n"
    routes += "    if len("+table+") < 1:\n"
    routes += "      flash('"+table+" is too short', category='error')\n"
    routes += "      return render_template('"+table+"/add.html', user=current_user)\n"
    routes += "    else:\n"
    routes += "      flash('"+table+" added!', category='success')\n"
    routes += "      new_"+table+" = "+model+"()\n"
    for field in fields:
      routes += "      new_"+table+"."+field+"="+field+"\n"
    routes += "      db.session.add(new_"+table+")\n"
    routes += "      db.session.commit()\n"
    routes += "    return redirect(url_for('"+table+"s."+table+"s_list'))\n"
    routes += "  else:\n"
    routes += "    return render_template('"+table+"s/add.html', user=current_user)\n\n"

    routes += "@"+table+"s.route('/"+table+"-list', methods=['GET'])\n"
    routes += "@login_required\n"
    routes += "def "+table+"s_list():\n"
    routes += "  user = current_user\n"
    routes += "  "+table+"s = "+model+".query.order_by("+model+".updated_at.desc()).all()\n"
    routes += "  return render_template('"+table+"s/list.html', "+table+"s="+table+"s ,user=user)\n\n"

    routes += "@"+table+"s.route('/"+table+"-edit/<"+table+"Id>', methods=['GET','POST'])\n"
    routes += "@login_required\n"
    routes += "def "+table+"_edit("+table+"Id):\n"
    routes += "  if request.method == 'GET':\n"
    routes += "    data = "+model+".query.get("+table+"Id)\n"
    routes += "    return render_template('"+table+"s/edit.html', data=data, user=current_user)\n"
    routes += "  else:\n"
    routes += "    data = "+model+".query.get("+table+"Id)\n"
    for field in fields:
      routes += "    "+field+" = request.form.get('"+field+"')\n"
    for field in fields:
      routes +=  "    data."+field+" =  "+field+"\n"
    routes += "    db.session.commit()\n"
    routes += "    flash('"+model+" updated!.', category='success')\n"
    routes += "    return redirect(url_for('"+table+"s."+table+"s_list'))\n\n"

    routes += "@"+table+"s.route('/"+table+"-del/<"+table+"Id>', methods=['GET','POST'])\n"
    routes += "@login_required\n"
    routes += "def "+table+"_del("+table+"Id):\n"
    routes += "  "+table+" = "+model+".query.get("+table+"Id)\n"
    routes += "  if "+table+": \n"
    routes += "    if request.method == 'GET':\n"
    routes += "      "+table+" = "+model+".query.get("+table+"Id)\n"
    routes += "      return render_template('"+table+"s/del.html', data="+table+", user=current_user)\n"
    routes += "    elif  request.method == 'POST':\n"
    routes += "      db.session.delete("+table+")\n"
    routes += "      db.session.commit()\n"
    routes += "      flash('"+model+" deleted!', category='success')\n"
    routes += "  return redirect(url_for('"+table+"s."+table+"s_list'))\n"

    return render_template('crud.html', user=current_user, add_html=add_html, edit_html=edit_html, del_html=del_html, list_html=list_html, blueprint=blueprint, import_blueprint=import_blueprint, register_blueprint=register_blueprint, routes=routes)
