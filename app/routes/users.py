from app import db
from app.models import User
from datetime import datetime
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for


# create Blueprint
users = Blueprint('users', __name__)

@users.route('/user-list', methods=['GET'])
@login_required
def user_list():
  if current_user.role == 'admin':
    user = current_user
    users = User.query.all()
    return render_template('users/list.html', users=users ,user=user)
  else:
    return redirect(url_for('contents.home'))

@users.route('/user-add', methods=['GET', 'POST'])
@login_required
def user_add():
  if current_user.role == 'admin':
    if request.method == 'POST':
      email = request.form.get('email')
      first_name = request.form.get('first_name')
      last_name = request.form.get('last_name')
      role = request.form.get('role')
      password1 = request.form.get('password1')
      password2 = request.form.get('password2')
      user = User.query.filter_by(email=email).first()

      if user:
        flash('Email already exists.', category='error')
      elif len(email) < 4:
        flash('Email must be greater than 4 characters.', category='error')
      elif len(first_name) < 2:
        flash('First name must be greater than 2 characters.', category='error')
      elif len(last_name) < 2:
        flash('Last name must be greater than 2 characters.', category='error')
      elif password1 != password2:
        flash('Passwords don\'t match.', category='error')
      elif len(password1) < 7 :
        flash('Passwords must be greater than 7 characters.', category='error')
      else:
        #add user to database
        new_user = User(email=email, first_name=first_name, last_name=last_name, role=role, password=generate_password_hash(password1))
        db.session.add(new_user)
        db.session.commit()
        flash('User added!', category='success')
        return redirect(url_for('users.user_list'))
      return render_template('users/add.html', user=current_user)
    else:
      return render_template('users/add.html', user=current_user)
  else:
    return redirect(url_for('contents.home'))

@users.route('/user-edit/<userId>', methods=['GET', 'POST'])
@login_required
def user_edit(userId):
  data = User.query.get(userId)
  if current_user.role == 'admin':
    if request.method == 'POST':
      email = request.form.get('email')
      first_name = request.form.get('first_name')
      last_name = request.form.get('last_name')
      role = request.form.get('role')
      password1 = request.form.get('password1')
      password2 = request.form.get('password2')
  
      if len(email) < 4:
        flash('Email must be greater than 4 characters.', category='error')
      elif len(first_name) < 2:
        flash('First name must be greater than 2 characters.', category='error')
      elif len(last_name) < 2:
        flash('Last name must be greater than 2 characters.', category='error')
      elif password1 != password2:
        flash('Passwords don\'t match.', category='error')
      else:
        #add user to database
        data.email = email
        data.first_name = first_name
        data.last_name = last_name
        data.role = role
        if password1 != "":
          data.password = generate_password_hash(password1)
        db.session.commit()
        flash('User updated!', category='success')
        return redirect(url_for('users.user_list'))
      return render_template('users/edit.html', data=data, user=current_user)
    else:
      return render_template('users/edit.html', data=data, user=current_user)
  else:
    return redirect(url_for('contents.home'))

@users.route('/user-del/<userId>', methods=['GET','POST'])
@login_required
def user_del(userId):
  if current_user.role == 'admin':
    data = User.query.get(userId)
    if data: 
      if request.method == 'GET':
        data = User.query.get(userId)
        return render_template('users/del.html', data=data, user=current_user)
      elif  request.method == 'POST':
        if data.role != 'admin':
          db.session.delete(data)
          db.session.commit()
          flash('User deleted!', category='success')
        else:
          flash('Role user is admin!', category='error')
    return redirect(url_for('users.user_list'))
  else:
    return redirect(url_for('contents.home'))