from flask import Flask
from sqlalchemy.orm import Session
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
DB_NAME = "database.sqlite" 

def create_app():
  # initialitation
  app = Flask(__name__)
  app.config['SECRET_KEY'] = '123456789qwertyasdf'
  app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'

  # create db
  from .models import User, Log
  db.init_app(app)
  with app.app_context():
    db.create_all()
    print("Created Database")

  # import blueprints
  from .routes.contents import contents
  from .routes.auth import auth
  from .routes.logs import logs
  from .routes.users import users
  from .routes.crud import cruds

  # register blueprint
  app.register_blueprint(contents, url_prefix='/')
  app.register_blueprint(auth, url_prefix='/')
  app.register_blueprint(logs, url_prefix='/')
  app.register_blueprint(users, url_prefix='/')
  app.register_blueprint(cruds, url_prefix='/')

  login_manager = LoginManager()
  login_manager.login_view = 'auth.login'
  login_manager.init_app(app)

  @login_manager.user_loader
  def load_user(id):
    return User.query.get(int(id))

  return app