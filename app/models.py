from . import db
from sqlalchemy.sql import func
from flask_login import UserMixin

class Log(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  log_type = db.Column(db.String(100), default="default")
  data = db.Column(db.String(1000))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

# UserMixin is used to implement login
class User(db.Model, UserMixin):
  id = db.Column(db.Integer, primary_key=True)
  email = db.Column(db.String(150), unique=True)
  password = db.Column(db.String(150))
  first_name = db.Column(db.String(150))
  last_name = db.Column(db.String(150))
  role = db.Column(db.String(150), default="user")
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  logs = db.relationship('Log')

  def __repr__(self):
    return f'<User: {self.email}>'