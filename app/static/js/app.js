function deleteNote(noteId){
  fetch('/delete-log', {
    method: 'POST',
    body: JSON.stringify({noteId: noteId}),
  }).then((_res)=>{
    window.location.href = "/log-list";
  });
}

$(document).ready(function () {
  // show the alert
  setTimeout(function () {
    $(".alert").alert('close');
  }, 2000);
});