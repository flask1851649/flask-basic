# Flash - Basic

- This is a basic application that has authentication and logging features.
- This application has features that differentiate the roles of users. There are 2 roles, namely admin and user.

## How to install
- Clone repository: https://gitlab.com/flask1851649/flask-auth.git
- create env according to requirements.txt

## How to run it
- Use the command: python main.py

## Fill in default data
- Use the command: python migrate.py

## Enter the application
- username: me@onestringlab.com
- password: secret

## Application interface
### Home
![image home](https://gitlab.com/flask1851649/flask-basic/-/raw/main/app/static/images/home.png?ref_type=heads)

### Login
![image login](https://gitlab.com/flask1851649/flask-basic/-/raw/main/app/static/images/login.png?ref_type=heads)

### Logs
![image logs](https://gitlab.com/flask1851649/flask-basic/-/raw/main/app/static/images/logs.png?ref_type=heads)